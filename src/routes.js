import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import Login from './screens/Login';
import Home from './screens/Home';

const Stack = createStackNavigator();

const basicOptions = () => ({
  headerShown: false,
});

const Routes = () => (
  <NavigationContainer>
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <React.Fragment>
        <Stack.Screen component={Login} options={basicOptions()} name="Login" />
        <Stack.Screen component={Home} options={basicOptions()} name="Home" />
      </React.Fragment>
    </Stack.Navigator>
  </NavigationContainer>
);

export default Routes;
