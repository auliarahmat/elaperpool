import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import Routes from './routes';

const Index = () => {
  useEffect(() => {
    setTimeout(SplashScreen.hide, 2000);
    StatusBar.setTranslucent(true);
    StatusBar.setBackgroundColor('transparent');
    StatusBar.setBarStyle('dark-content', true);
  }, []);

  return <Routes />;
};

export default Index;
