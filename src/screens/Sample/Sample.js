import React from 'react';
import { Text, View } from 'react-native';

const Sample = () => {
  return (
    <View>
      <Text>Hello sample</Text>
    </View>
  );
};

export default Sample;
