import React from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  ScrollView,
  TextInput,
  Image,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import styles from './styles';

const Home = () => {
  const category = [
    {
      id: 1,
      name: 'Pizza',
      image:
        'https://cdn.popbela.com/content-images/post/20201210/40f0dd5981c7197e18d8865fd0bed568_750x500.jpg',
    },
    {
      id: 2,
      name: 'Burger',
      image: 'https://statik.tempo.co/data/2018/10/01/id_737545/737545_720.jpg',
    },
    {
      id: 3,
      name: 'Pizza',
      image:
        'https://cdn.popbela.com/content-images/post/20201210/40f0dd5981c7197e18d8865fd0bed568_750x500.jpg',
    },
    {
      id: 4,
      name: 'Burger',
      image: 'https://statik.tempo.co/data/2018/10/01/id_737545/737545_720.jpg',
    },
  ];

  const trending = [
    {
      id: 1,
      name: 'Pizza King',
      price: 45,
      size: '270 Kcal',
      image:
        'https://content3.jdmagicbox.com/comp/pune/h6/020pxx20.xx20.180831210448.w3h6/catalogue/sairaj-pizz-center-bangarwadi-pune-fast-food-1pnqtqmr49.jpg',
    },
    {
      id: 1,
      name: 'Burger',
      price: 25,
      size: '270 Kcal',
      image: 'https://statik.tempo.co/data/2018/10/01/id_737545/737545_720.jpg',
    },
  ];
  return (
    <>
      <StatusBar />
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <Feather name="menu" size={25} color="grey" />
          <Feather name="filter" size={25} color="grey" />
        </View>

        <View style={styles.containerSearch}>
          <View style={styles.containerIcon}>
            <Feather name="search" size={20} color="grey" />
          </View>
          <TextInput placeholder="Search" style={styles.search} />
        </View>

        <ScrollView>
          <View style={styles.banner}>
            <Image
              resizeMode="cover"
              style={styles.image}
              source={{
                uri: 'https://img.freepik.com/free-vector/healthy-restaurant-banner_23-2148664891.jpg?size=626&ext=jpg',
              }}
            />
          </View>

          <View style={styles.containerCategories}>
            <Text style={styles.title}>Categories</Text>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {category.map((item, idx) => {
                return (
                  <View
                    key={idx}
                    style={[
                      styles.categories,
                      idx === 0 ? { backgroundColor: 'red' } : null,
                    ]}>
                    <View style={styles.imgCategories}>
                      <Image style={styles.img} source={{ uri: item.image }} />
                    </View>
                    <View style={styles.textCategories}>
                      <Text>{item.name}</Text>
                    </View>
                  </View>
                );
              })}
            </ScrollView>
          </View>

          <View style={styles.trending}>
            <Text style={styles.title}>Trending</Text>
            <View style={styles.containerTrending}>
              {trending.map((item) => {
                return (
                  <View style={styles.contentTrending}>
                    <View style={styles.containerImageTrending}>
                      <Image
                        source={{
                          uri: item.image,
                        }}
                        style={styles.image}
                      />
                    </View>
                    <Text style={styles.titleContentTrending}>{item.name}</Text>
                    <View style={styles.containerPrice}>
                      <View style={styles.column}>
                        <Text style={styles.textPrice}>{item.size}</Text>
                      </View>
                      <View style={styles.rowPrice}>
                        <Text>$</Text>
                        <Text style={[styles.textPrice, { fontSize: 23 }]}>
                          {item.price}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Home;
