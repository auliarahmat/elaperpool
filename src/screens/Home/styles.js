import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20,
  },
  header: {
    marginTop: 15,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  containerSearch: {
    flexDirection: 'row',
    height: 45,
    width: '100%',
    backgroundColor: 'rgb(245, 245, 245)',
    borderRadius: 12,
    borderWidth: 0.3,
    borderColor: 'grey',
  },
  containerIcon: {
    width: 40,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  search: {
    fontSize: 15,
  },
  banner: {
    height: 160,
    marginTop: 20,
  },
  image: {
    height: '100%',
    width: '100%',
    borderRadius: 15,
  },
  containerCategories: {
    marginTop: 20,
  },
  categories: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 140,
    borderWidth: 0.5,
    borderColor: 'grey',
    borderRadius: 50,
    margin: 7,
  },
  title: {
    fontSize: 17,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  imgCategories: {
    width: 30,
    height: 30,
    borderRadius: 50,
    marginHorizontal: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    height: '80%',
    width: '80%',
    borderRadius: 50,
  },
  textCategories: {
    flex: 1,
    justifyContent: 'center',
  },
  trending: {
    marginTop: 20,
  },
  containerTrending: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentTrending: {
    width: 160,
    backgroundColor: 'white',
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 5.32,
    elevation: 16,
    marginBottom: 10,
    borderRadius: 20,
  },
  containerImageTrending: {
    height: 130,
    width: '100%',
  },
  titleContentTrending: {
    padding: 5,
    fontSize: 16,
    fontWeight: '600',
  },
  containerPrice: {
    paddingHorizontal: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rowPrice: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textPrice: {
    fontWeight: 'bold',
  },
});
