import React from 'react';
import { ScrollView, Image, View, TextInput } from 'react-native';
import { Button, Text } from 'react-native-elements';

const Login = ({ navigation }) => {
  return (
    <ScrollView>
      <View style={{ paddingTop: 32 }}>
        <View style={{ alignItems: 'center' }}>
          <Image
            style={{ height: 230, width: 230 }}
            source={require('../../assets/images/elaperpool-transparent.png')}
          />
          <Text
            style={{
              marginTop: 24,
              textAlign: 'center',
              fontFamily: 'Lato-Bold',
              fontSize: 24,
              color: '#4f4f4f',
            }}>
            Welcome to E-Laperpool!
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: 'Lato-Regular',
              fontSize: 14,
              color: '#4f4f4f',
              marginTop: 16,
            }}>
            Don't let your stomatch growl
          </Text>
        </View>
        <View style={{ marginHorizontal: 16, marginTop: 48 }}>
          <TextInput
            placeholder="mail@example.com"
            keyboardType="email-address"
            style={{
              backgroundColor: '#e8e8e8',
              paddingHorizontal: 16,
              paddingVertical: 14,
              borderRadius: 4,
              fontFamily: 'Lato-Bold',
            }}
          />
          <TextInput
            placeholder="•••••••••"
            keyboardType="default"
            secureTextEntry
            style={{
              marginTop: 12,
              backgroundColor: '#e8e8e8',
              paddingHorizontal: 16,
              paddingVertical: 14,
              borderRadius: 4,
              fontFamily: 'Lato-Bold',
            }}
          />
        </View>
        <View style={{ marginHorizontal: 16, marginTop: 48 }}>
          <Button
            title="Login"
            buttonStyle={{
              backgroundColor: '#F54747',
              paddingVertical: 14,
            }}
            titleStyle={{ fontFamily: 'Lato-Bold' }}
            onPress={() => navigation.navigate('Home')}
          />
          <Text
            style={{
              marginTop: 24,
              textAlign: 'center',
              fontFamily: 'Lato-Regular',
              fontSize: 12,
              color: '#4f4f4f',
            }}>
            Tidak memiliki akun?{' '}
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'Lato-Bold',
                fontSize: 12,
                color: '#F54747',
              }}
              onPress={() => alert('ok')}>
              Daftar Sekarang
            </Text>
          </Text>
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;
