import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const Header = (props) => {
  const {
    title,
    children,
    onBackButtonPress,
    transparent,
    customContainerStyle,
    withBackButton,
  } = props;
  return (
    <React.Fragment>
      <View style={[styles.mainContainer(transparent), customContainerStyle]}>
        <View style={styles.itemContainer}>
          {withBackButton && (
            <TouchableOpacity
              style={styles.containerButton}
              onPress={() => {
                onBackButtonPress();
              }}>
              <Image
                source={require('../../assets/icons/icon_back_black.png')}
              />
            </TouchableOpacity>
          )}
          <Text style={styles.textTitle}>{title}</Text>
        </View>
      </View>
      {children}
    </React.Fragment>
  );
};

export default Header;

Header.propTypes = {
  onBackButtonPress: PropTypes.func,
};

Header.defaultProps = {
  onBackButtonPress: () => {},
};
