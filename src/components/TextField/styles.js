import {
  COLOR_LIGHT_GREY,
  COLOR_LIGHTER_GREY,
  FONT_CAPTION_PRIMARY,
  COLOR_BLACK,
} from '@styles';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    ...FONT_CAPTION_PRIMARY,
    color: COLOR_BLACK,
    marginBottom: 6,
  },
  textInputContainer: {
    flexDirection: 'row',
    backgroundColor: COLOR_LIGHTER_GREY,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: COLOR_LIGHT_GREY,
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    height: 43,
  },
});
