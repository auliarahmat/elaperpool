import React, { useState } from 'react';
import { Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import styles from './styles';

const TextField = (props) => {
  const {
    type,
    keyboardType,
    placeholder,
    onChangeText,
    title,
    value,
    disabled,
  } = props;
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  return (
    <View>
      <Text style={styles.container}>{title}</Text>
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          onChangeText={onChangeText}
          value={value}
          placeholder={placeholder}
          editable={disabled}
          autoCapitalize="none"
          keyboardType={keyboardType || 'default'}
          secureTextEntry={type === 'password' ? !isPasswordVisible : false}
        />
        {type === 'password' && (
          <TouchableOpacity
            onPress={() => setIsPasswordVisible(!isPasswordVisible)}>
            <Image source={require('../../images/icon_eye_disable.png')} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default TextField;
